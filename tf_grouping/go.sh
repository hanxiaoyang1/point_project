/usr/local/cuda/bin/nvcc group.cu -o tf_grouping_g.cu.o -c -O2 -DGOOGLE_CUDA=1 -x cu -Xcompiler -fPIC -I/workdir/anaconda3/envs/tf2/lib/python3.9/site-packages/tensorflow/include --expt-relaxed-constexpr
TF_CFLAGS=( $(python -c 'import tensorflow as tf; print(" ".join(tf.sysconfig.get_compile_flags()))') )
TF_LFLAGS=( $(python -c 'import tensorflow as tf; print(" ".join(tf.sysconfig.get_link_flags()))') )
g++ -std=c++11 -shared group.cpp tf_grouping_g.cu.o -o out.so -fPIC ${TF_CFLAGS[@]} ${TF_LFLAGS[@]} -O2 -I /usr/local/cuda/include -lcudart -L /usr/local/cuda/lib64/ 